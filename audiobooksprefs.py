from gi.repository import Gio, GObject, PeasGtk, Gtk
import rb
import os

class AudiobooksPrefs(GObject.Object, PeasGtk.Configurable):
    __gtype_name__ = 'booksConfigDialog'
    object = GObject.property(type=GObject.Object)

    def __init__(self):
        GObject.Object.__init__(self)
        self.settings = Gio.Settings.new('org.gnome.rhythmbox.plugins.audiobooks')

    def do_create_configure_widget(self):

        user = os.environ.get('USER')
        path = "/home/" + user + \
            "/.local/share/rhythmbox/plugins/rhythmbox-books/audiobooks-prefs.ui"

        builder = Gtk.Builder()
        builder.add_from_file(rb.find_plugin_file(self, path))

        config = builder.get_object("config")

        switch = builder.get_object("scan")
        directory = builder.get_object("directory")

        switch.set_active(self.settings["remove-entries"])
        switch.connect("notify::active", self.switch_toggled, "remove-entries")

        directory.set_current_folder(self.settings["books-folder"])
        directory.connect("current-folder-changed", self.set_directory)

        return config

    def switch_toggled(self, switch, active, key):
        self.settings[key] = switch.get_active()

    def set_directory(self, file_chooser):
       self.settings["books-folder"] = file_chooser.get_current_folder()

