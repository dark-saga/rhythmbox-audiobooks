from gi.repository import RB, Gtk, Gdk, GLib
import datetime
import os
import re
from mutagen.easyid3 import EasyID3
from mutagen.mp3 import MP3
from mutagen.flac import FLAC

class Book:

    def __init__(self, track_number, title, genre, artist, album, year, duration, comment):

        self.track_number = track_number
        self.title = title
        self.genre = genre
        self.artist = artist
        self.album = album
        self.year = year
        self.duration = duration
        self.comment = comment


class BooksSource(RB.BrowserSource):
    def __init__(self):
        RB.BrowserSource.__init__(self)

        self.activated = False
        self.entryView = self.get_entry_view()

       
    def do_selected(self):

        if not self.activated:
            self.activated = True

            self.entry_view = self.get_entry_view()
            self.entry_view.get_column(
                RB.EntryViewColumn.ALBUM).set_title("Book")
            self.entry_view.get_column(
                RB.EntryViewColumn.ARTIST).set_title("Author")

            self.entry_view.set_column_editable(RB.EntryViewColumn.ALBUM, True)
            self.entry_view.set_sorting_order("Track", Gtk.SortType.ASCENDING)

            self.props.show_browser = True

            self.property_views = self.get_property_views()

            # artist.set_min_content_height(265)

            self.run_idle()

            temp = vars(self)
            # for item in temp:
            #     print (item + ' : ' + temp[item])

            attrs = vars(self)
            print(', '.join("%s: %s" % item for item in attrs.items()))

            custom_pv = RB.PropertyView.new(self.props.plugin.db, 3, "Book")
            self.pack_end(custom_pv)

    def do_impl_delete_thyself(self):
        if self.activated:
            self.shell = False

    def run_idle(self):

        Gdk.threads_init()

        Gdk.threads_add_idle(GLib.PRIORITY_DEFAULT_IDLE,
                             self.check_deleted_files)
        Gdk.threads_add_idle(GLib.PRIORITY_DEFAULT_IDLE,
                             self.scan_books_folder)
        Gdk.threads_add_idle(GLib.PRIORITY_DEFAULT_IDLE, self.monitor_tags)

    def add_book(self, book, entry):

        if book.track_number:
            self.props.plugin.db.entry_set(
                entry, RB.RhythmDBPropType.TRACK_NUMBER, book.track_number)

        if book.title:
            self.props.plugin.db.entry_set(
                entry, RB.RhythmDBPropType.TITLE, book.title)

        if book.genre:
            self.props.plugin.db.entry_set(
                entry, RB.RhythmDBPropType.GENRE, book.genre)

        if book.artist:
            self.props.plugin.db.entry_set(
                entry, RB.RhythmDBPropType.ARTIST, book.artist)

        if book.album:
            self.props.plugin.db.entry_set(
                entry, RB.RhythmDBPropType.ALBUM, book.album)

        if book.year:
            self.props.plugin.db.entry_set(
                entry, RB.RhythmDBPropType.DATE, datetime.date(book.year, 1, 1).toordinal())

        if book.comment:
            self.props.plugin.db.entry_set(
                entry, RB.RhythmDBPropType.COMMENT, book.comment)

        if book.duration:
            self.props.plugin.db.entry_set(
                entry, RB.RhythmDBPropType.DURATION, book.duration)

        self.props.plugin.db.commit()
        self.props.query_model.add_entry(entry, -1)

    def scan_books_folder(self):

        uri = self.props.plugin.config.settings["books-folder"]

        for root, dirs, files in os.walk(uri):
            for file in files:
                if file.endswith(".mp3") or file.endswith(".flac"):

                    uri = "file://" + root + "/" + file

                    if not self.props.plugin.db.entry_lookup_by_location(uri):

                        # EasyID3.RegisterTextKey('comment', 'COMM')

                        if file.endswith(".mp3"):
                            audio = EasyID3(root + "/" + file)
                        else:
                            audio = FLAC(root + "/" + file)

                        try:
                            tracknumber = int(
                                re.search('^[0-9]+', audio["tracknumber"][0])[0])
                        except KeyError:
                            tracknumber = None

                        try:
                            title = audio["title"][0]
                        except KeyError:
                            title = None

                        try:
                            genre = audio["genre"][0]
                        except KeyError:
                            genre = None

                        try:
                            artist = audio["artist"][0]
                        except KeyError:
                            artist = None

                        try:
                            album = audio["album"][0]
                        except KeyError:
                            album = None

                        try:
                            year = int(audio["date"][0])
                        except KeyError:
                            year = None

                        try:
                            comment = audio["comment"][0]
                        except KeyError:
                            print(KeyError, audio)
                            comment = None

                        if file.endswith(".mp3"):
                            audio = MP3(root + "/" + file)

                        length = int(audio.info.length)
                        bitrate = int(audio.info.bitrate/1000)

                        book = Book(
                            tracknumber,
                            title,
                            genre,
                            artist,
                            album,
                            year,
                            length,
                            comment
                        )

                        entry = RB.RhythmDBEntry(
                            self.props.plugin.db, self.props.plugin.entry_type, uri)
                        self.add_book(book, entry)

        Gdk.threads_add_timeout(
            GLib.PRIORITY_DEFAULT_IDLE, 1000, self.scan_books_folder)

    def check_deleted_files(self):

        for book in self.props.query_model:

            uri = book[0].get_string(RB.RhythmDBPropType.LOCATION)
            location = re.sub("^[/]+", "/", uri.__str__()[6:])

            if not (os.path.isfile(location)) and self.props.plugin.config.settings["remove-entries"]:
                self.props.plugin.db.entry_delete(book[0])
                self.props.plugin.db.commit()
                # self.props.query_model.remove_entry(book[0])

        Gdk.threads_add_timeout(
            GLib.PRIORITY_DEFAULT_IDLE, 1000, self.check_deleted_files)

    def monitor_tags(self):

        for book in self.props.query_model:

            uri = book[0].get_string(RB.RhythmDBPropType.LOCATION)
            location = re.sub("^[/]+", "/", uri.__str__()[6:])

            # audio = EasyID3(location)

            if location.endswith(".mp3"):
                audio = EasyID3(location)
            else:
                audio = FLAC(location)

            tags = {
                "title": RB.RhythmDBPropType.TITLE,
                "genre": RB.RhythmDBPropType.GENRE,
                "artist": RB.RhythmDBPropType.ARTIST,
                "album": RB.RhythmDBPropType.ALBUM,
                "comment": RB.RhythmDBPropType.COMMENT,
            }

            for k, v in tags.items():

                try:
                    original = audio[k][0]

                    if original is not book[0].get_string(v):
                        self.entry_set(self.props.plugin.db,
                                        book[0], v, original)
                except Exception as e:
                    print("EXCEPTION - " + e.__str__())

            # year
            try:
                original = datetime.date(
                    int(audio["date"][0]), 1, 1).toordinal()

                if original is not book[0].get_ulong(RB.RhythmDBPropType.DATE):
                    self.entry_set(self.props.plugin.db,
                                    book[0], RB.RhythmDBPropType.DATE, original)
            except Exception as e:
                print("EXCEPTION - " + e.__str__())
                print(book[0].get_string(RB.RhythmDBPropType.TITLE))
                print(book[0].get_string(RB.RhythmDBPropType.ALBUM))

            # tracknumber
            try:
                original = int(
                    re.search('^[0-9]+', audio["tracknumber"][0])[0])

                if original is not book[0].get_ulong(RB.RhythmDBPropType.TRACK_NUMBER):
                    self.entry_set(
                        self.props.plugin.db, book[0], RB.RhythmDBPropType.TRACK_NUMBER, original)
            except Exception as e:
                print("EXCEPTION - " + e.__str__())


        Gdk.threads_add_timeout(
            GLib.PRIORITY_DEFAULT_IDLE, 5000, self.monitor_tags)

    def entry_set(self, db, entry, propid, value):
        db.entry_set(entry, propid, value)
        db.commit()